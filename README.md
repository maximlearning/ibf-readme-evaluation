# Documentation for Red Cross Netherlands regarding Azure Functions Pipeline.
This document counts as a guide for the creation and deployment of your first Azure Function as well as documentation for the IbF serverless pipeline project for the Red Cross.  

## Table of Contents
- [Scope of the project](#scope-of-the-project)
- [What is Azure Functions?](#what-is-azure-functions)
- [Explanation of the pipeline](#explanation-of-the-pipeline)
- [Integration with Visual Studio Code](#integration-with-visual-studio-code)
- [Debugging locally](#debugging-locally)
- [Deploying your function to Azure](#deploying-your-function-to-azure)
- [The Azure environment](#the-azure-environment)
- [Appendix](#appendix)

## Scope of the project

Red Cross Netherlands wants to rewrite the existing trigger model pipeline for Zambia, such that the result will be easier to understand and more modular than the existing one. The pipeline needs to be ‘serverless’, meaning that the backend maintenance is handled by Azure. The serverless solution is run on Azure as this is the preferred could provider of Red Cross Netherlands. Azure Functions is the serverless solution of Azure and therefor has been used in this project.

## What is Azure Functions?

Azure Functions is a service from Azure that allows to run relatively small pieces of code without the need for a virtual machine (VM). This means a lot fewer back-end maintenance and, since all functions consist only of relatively small pieces of code, easier to change only part of the whole pipeline.
Also, since there is a ‘serverless’ payment subscription, Red Cross only pays for the time the functions are actually running, reducing the costs significantly when compared to a VM (or other non-serverless instances).

## Explanation of the pipeline

From this point onward, when we refer to the capitalized word Functions, we refer to Azure Functions. Otherwise we refer to functions used within Python code.

The serverless pipeline consists out of four Functions (figure 1), each with a small and specific goal. The existing pipeline already contains code and functions in alignment with this strategy. The existing functions from that pipeline have each been assigned to a separate Azure Function. Next, the contents each of the Functions will be described in chronical order.

**FTP2Blob:** <br>
In this function raw zip files are pulled from the Glofas remote FTP server. The zip files are opened and the required files are written to Azure blobstorage. Hence the name FTP2Blob.

**RemoveOldGlofasData:**<br>
The history of GloFAS files builds up over time. This Function removes old data from the blobstorage  based on a threshold value (older than X days, months, years).

**BlobTriggerExtract:**<br>
The most recent GloFAS data is imported from the blobstorage and the reference threshold table is imported from the Postgresql table. The extract function uses the data to make two (x51) predictions per station. One for T+3 days and one for T+7 days. This results in a .json file for each of the corresponding timeframes. The .json files are written to azure blobstorage.

**findTrigger:**<br>
In the findTrigger Function, the .json files from the BlobTriggerExtract Function are loaded and compared with a given return period to obtain the final results. These final results are written to a table in the Postgresql database.

**teamswebhook:**<br>
Each of the previous functions is surrounded by a try-except statement which report Python error messages to teams when an error occurs. An important note is that this teamswebhook is created through a python module and is in no way dependent on Azure Functions. The same type of teams reporting is possible in any other solution.

Figure 1: Schematic overview of the pipeline
![Figure 1: Schematic overview of the pipeline](Pictures/pipeline_schema.png)

## Integration with Visual Studio Code

Azure has a very good integration with Visual Studio Code (VSC) and developing and deploying is easily done using VSC. We therefor will explain how developing and deploying works in VSC, but note that it is also very well possible to do this with other IDE’s.

Before we continue, you will have to set up an function app resource in your resourcegroup in the azure portal. In your resourcegroup click on '+ Add' to add a resource. Here you will see the Function App depicted by a lightning strike in the popular section. Add this resource to your resourcegroup.
Fill in the required information:
```
- Publish: Code
- Runtime stack: Python
- Region: West Europe
```

Furthermore, we assume that you have:

-	Follow the initiating process by specifying region, pricing, and connecting a (new) storage account that will be used by the app.
-	Installed Visual Studio Code (https://code.visualstudio.com/download).
-	Installed the Python, Azure Account, and Azure Functions extensions in Visual Studio Code.
-	Installed  NodeJs and NPM on your laptop (https://nodejs.org/en/download/) <br> This is required if you want to test your app locally. For some programming languages other than Python live editing in the Azure Portal is available, however for working with Python the local interaction with VSC is mandatory.
- You have a virtual environment with a supported Python version. As of now this is Python 3.6 or 3.7.

If we have done that, we can continue with our functions:
- Initiate a new function by clicking on the 'create new function' button in the VSC Azure Functions extension.
- Follow the instructions by submitting the following parameters:
```
  - language = python
  - trigger = blobtrigger
  - name = **insert-your-super-clear-function-name-here**
  - authentication = anonymous
  - Click 'yes' if prompted to change current folder to an Azure Functions folder.
```

Azure will now have added some files to your folder, such as `function.json`, `__init__.py`, `local_settings.json` , etc.
These files will become the backbone of your app. There are two files we need to dive into deeper: the `__init__.py` and the `function.json`.

`__init__.py` <br>
This file is the main coding part of the Function. If all previous steps have gone correctly, the main function will look like the following:

```python
import logging

import azure.functions as func

def main(myblob: func.InputStream):
  logging.info(f"Python blob trigger function processed blob \n"
               f"Name: {myblob.name}\n"
               f"Blob Size: {myblob.length} bytes")
```

This is all the BlobTrigger template provides. You can add the code that needs to be run below, within the main – function.
An interesting part of this template is the ‘myblob’ – variable. This is your trigger; in this case, if whatever ‘myblob’ is representing is changed, the main() – function will run.

How the trigger is set up can be found in the other import file, the `function.json`. Its template looks as follows.

```json
{
  "scriptFile": "__init__.py",
  "bindings": [
    {
      "name": "myblob",
      "type": "blobTrigger",
      "direction": "in",
      "path": "samples-workitems/{name}",
      "connection": "AzureWebJobsStorage"
    }
  ]
}
```
First of all, it should be noted that the scriptFile – argument is telling us what file is searched for the main() – function. You do not have to do anything with this, but it may be useful when you have another file that is not called `__init__.py` (containing a main() – function) and want to run that.

The most important parts of this file are the bindings. Bindings are any connection the Function has with the outside world: the input (can be files, but also triggers) and output.
The ‘path’ is where the trigger is looking for changes. To work with this you need to
-	change ‘samples-workitems’ to the container you are interested in
-	change {name} to the file you are interested in.

The {name} parameter makes the blob trigger very flexible. If you change it, for example, to {name}.csv the code will run only if a change in a csv-file in the container is made. If you change {name} to ‘test.csv’, only changes in ‘test.csv’ will trigger the Function. This also means that with {name} any change in a file is enough to trigger the Function.

It should be noted that, in this pipeline, only triggers are included in bindings. This is done because you have to define a binding per file you read or write. As we have function reading or writing 115 files, this is not feasible. Therefor, a different route is taken.
In these functions, the Python-package that allows us to connect to a Storage Account in Azure is used. This means we do not have to specify all bindings manually, but can loop over elements in the Storage Account. However, it also means we have to install another package, slowing the code down.

### Timer Trigger example
To see how all these settings work together in a Function, we show a piece of code from the FTP2Blob Function included in the pipeline, but slightly simplified.
```python
import datetime
import logging
import azure.functions as func
import azure.storage.blob

def makeFtpRequest():
    # Create location to extract files to
    os.mkdir('files')
    urllib.request.urlretrieve(ftp_path + filename, 'files'+filename)

    # Extract only the files we want to use later on -nc files
    tar = tarfile.open('files/'+filename, "r:gz")
    nc_files = [f for f in tar.getmembers() if f.name.endswith('.nc')]
    os.chdir('files')
    tar.extractall(members=nc_files)
    tar.close()
    logging.info('Downloading and extracting done.')

def write2Blobs(conn_str = os.environ['AzureWebJobsStorage'],
                location = 'files'):
    # Instantiate blob storage account
    blobService = BlobServiceClient.from_connection_string(conn_str = conn_str)
    for f in os.listdir():
        local_file_name = str(f)
        blob_client = blobService.get_blob_client(container='nc-files', blob=local_file_name)
        # If file already open, we have a problem. Luckily Functions are stateless.
        with open(f, "rb") as data:
            blob_client.upload_blob(data, overwrite = True)
        logging.info(f'Writing {str(f)} succesfull')

def main(mytimer: func.TimerRequest) -> None:
    '''Function to extract FTP-files and save them in blob storage'''
    utc_timestamp = datetime.datetime.utcnow().replace(
        tzinfo=datetime.timezone.utc).isoformat()

    if mytimer.past_due:
        logging.info('The timer is past due!')

    makeFtpRequest()
    logging.info('Downloading file from FTP-server done.')
    write2Blobs()
    logging.info('Writing to blob done.')

    logging.info('Python FTP2Blob function ran at %s', utc_timestamp)
```

It can be seen that we simply write our code inside the main() – function and that we use an external Python package (azure.storage.blob) to deal with blob storage.
In more detail, we have three functions: write2Blobs(), makeFtpRequest(), and main(). The first two functions are defined to help, but Azure looks specifically for the main() – function to run, when it is triggered. How is it triggered? We can find that in the `function.json`:

```json
{
  "scriptFile": "__init__.py",
  "bindings": [
    {
      "name": "mytimer",
      "type": "timerTrigger",
      "direction": "in",
      "schedule": "0 0 12 * * *"
    }
  ]
}
```

The function is scheduled by a CRON statement, in this case daily at 12:00.

### BlobTrigger example
The following is a simplified version of the BlobTriggerExtract Function that is part of the pipeline. It shows the central elements as discussed above, and is more involved than the timer trigger.

```python
def main(myblobtrigger: func.InputStream):
    try:
        logging.info(f"Python blob trigger function processed blob \n"
                    f"Name: {myblobtrigger.name}\n"
                    f"Blob Size: {myblobtrigger.length} bytes")

        # Get new dictionary for FORECASTS_STEPS
        # N.B. We need some very thorough error handling/ testing here.
        a,b,c,d = os.environ["FORECASTS_STEPS"].split(':')
        forecast_dict = {a:int(b), c:int(d)}
        for fcStep, days in forecast_dict.items():
            logging.info("Started processing glofas data: " + fcStep)

            # Load (static) threshold values per station
            blobService = BlobServiceClient.from_connection_string(conn_str = os.environ['AzureWebJobsStorage'])
            with open('/tmp/Glofas_station_locations_with_trigger_levels.csv', 'wb') as download_threshold:
                download_threshold.write(blobService.get_blob_client(container ='thresholds', blob = 'Glofas_station_locations_with_trigger_levels.csv').download_blob().readall())
            df_thresholds = pd.read_csv('/tmp/Glofas_station_locations_with_trigger_levels.csv', delimiter=';', encoding="windows-1251")
            df_thresholds.index = df_thresholds['station_code']
            df_thresholds.sort_index(inplace=True)

            # Load extracted Glofas discharge levels per station
            with open('/tmp/glofas_forecast_' + fcStep + '.json', 'wb') as download_forecasts:
                download_forecasts.write(blobService.get_blob_client(container ='glofas-forecasts', blob = 'glofas_forecast_' + fcStep + '.json').download_blob().readall())
            df_discharge = pd.read_json('/tmp/glofas_forecast_' + fcStep + '.json')
            df_discharge.index = df_discharge['code']
            df_discharge.sort_index(inplace=True)

            # Merge two datasets
            df = pd.merge(df_thresholds, df_discharge, left_index=True,
                            right_index=True)  # on=['station_code','code'])

            # Write results to blob storage
            out = df.to_json(orient='records')
            with open('/tmp/triggers_rp_'+ fcStep + '.json', 'w') as fp:
                fp.write(out)

            blob_client = blobService.get_blob_client(container='triggers-rp-per-station', blob='triggers_rp_' + fcStep + '.json')        
            with open('/tmp/triggers_rp_' + fcStep + '.json', 'rb') as data:
                blob_client.upload_blob(data, overwrite = True)
            logging.info("Processed Glofas data - file saved to JSON")
```

The code itself comes from an existing pipeline and we will not go into detail about that. First of all, we see our trigger: `myblobtrigger`, so we can conclude the Function will run if there is some change in a certain blob. Then the next interesting thing that can be seen is the `os.environ[]`. This will be explained shortly after, but in essence, this is the method to provide your code with your settings (via a string, hence the alterations on the settings).

There also are two examples of how we can use the `azure.storage.blob`-package to read and write files.
And also very important, note that all paths start with `/tmp/`. This is because writing access is only available to the `/tmp/` folder inside the Function.
As always, the `__init__.py` is not the only file that is needed to comprehend the Function. The `function.json` must be inspected as well:
```json
{
  "scriptFile": "__init__.py",
  "bindings": [
    {
      "name": "myblobtrigger",
      "type": "blobTrigger",
      "direction": "in",
      "path": "glofas-forecasts/glofas_forecast_long.json",
      "connection": "AzureWebJobsStorage"
    }
  ]
}
```
From this it can be seen that we are indeed looking at a blob trigger, and that it is triggered by a change in a certain json file.

## Debugging locally
We have to make some changes in your environment to be able to make your app to run locally, which we do by the following:
- Adapt local_settings.json by specifying the storage account: "AzureWebJobsStorage" : "your_own_connections_string_to_storage_account"
- Press F5 to run your app locally
    - VSC will now open a port on localhost for you, and provide you with the url to access your function.

In some cases, starting the VSC debugger by using F5 results in an error. If this is the case, another possibility to test the code locally is to activate the virtual environment the code uses (this can be seen inside the folder all your code is in), and use ‘func host start’ inside the virtual environment.

## Deploying your function to Azure
Now that you know for sure that the function works locally, it is time to deploy it to Azure and make it accessible via the web.

It is important that you add a Requirements.txt in the folder next to the local_settings file. This is in the folder outside of the individual functions. This file will automatically be used by Azure when setting up a Python environment.

Next, in the Azure Functions extension in VSC, click 'Deploy Function App' and select the Function App it should deploy to. This is the Function App that you created in the beginning of this tutorial in the Azure Portal. That is all you need to do in order to deploy from VSC to Azure.

It is important to note that your Azure Functions version locally and in Azure are the same. Currently, there are two versions used: 2 and 3. The specific differences between these versions are not of importance for now.

In some cases you may need to fill in some more settings, but these should fill out easily.

### Environment variables
When a Function is deployed to Azure, the settings or secrets file with your environment variables is not compatible with Azure Functions. In order to use these variables, you can add them in the Azure portal. Next, add for example `os.environ["GLOFAS_USER"]` to your script in order to use the GLOFAS_USER environment variable.

![Figure 2:Environment Variables](Pictures/environment_variables.PNG)

## The Azure environment

In order to view the project on Azure, log in to portal.azure.com. A resourcegroup called:
510Global-Datateam-EY should be available to you. If this is not the case, please contact the 510 IbF team for access tot his project. In this resourcegroup are a couple of resources related to the project.

![Figure 3: Resoucegroup Contents](Pictures/resourcegroup_contents.PNG)

**App service plan:** <br>
ASP-510GlobalDatateamEY-9b7b is the app service plan, with which you will have little interaction. This service plan is automatically made when a Azure Functions app service is created.

**Storage account:** <br>
Storageaccount510glad95 is the Azure blobstorage account in which raw files are written.
Inside the storage account click on containers. There should be around eight containers visible.
In each of these containers, data is written in as a blob. Containers can be viewed as directories. They are nonhierarchical and therefor it is not possible to put a container in a container. As mentioned earlier, reading and writing to the blobstorage can be done from Python using the module azure.storage.blob.

![Figure 4: Blobstorage Containers](Pictures/blobstorage_containers.PNG)

**App service (lightning icon):** <br>
The app service is the resource where all the code and associated files for all deployed Azure Functions is stored. Inside the app service on the left click on Functions. You can view the deployed functions and explore them further. From this location, Functions can be enabled or disabled. When pushing new Functions to azure from Visual Studio Code, the pushed Functions will automatically be enabled.

![Figure 5: Functionapp Functions](Pictures/functionapp_functions.PNG)

**Application insights:** <br>
The lightbulb icon depicts the application insights where logs are stored and a visual dashboard is available to see whether a process has failed.

![Figure 6: Appinsights Dashboard](Pictures/appinsights_dashboard.PNG)

When diving deeper into failed requests, the original error message can be found.

![Figure 7: Appinsights Failures](Pictures/appinsights_failures.PNG)

![Figure 8: Appinsights Error](Pictures/appinsights_error.PNG)

## Appendix
One of the first things you come across when doing tutorial about Azure Functions, but that are missing in this pipeline, are the file bindings. It is already mentioned that bindings are the connection of a Function to the outside world, and that input and output files should be defined here. An example of how to use these is provided below (where we also show another trigger possibility, namely an HTTP-trigger).

```python
# __init__.py
# Note that we have to define the bindings in the main()-function.
def main(req: func.HttpRequest,
        inputblob: func.InputStream,
        outputblob: func.Out[func.InputStream]) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    name = req.params.get('name')
    if not name:
        try:
            req_body = req.get_json()
        except ValueError:
            pass
        else:
            name = req_body.get('name')

    if name:
        # Read in the input binding.
        file = inputblob.read().decode('utf-8')
        df = pd.read_csv(io.StringIO(file), sep = ';')
        # And write the file to the ouput binding.
        outputblob.set(df.to_csv(sep = ';'))
        text_response = f"Hello {name}!\rWriting CSV to blob storage succeeded!"
        return func.HttpResponse(text_response)
    else:
        return func.HttpResponse(
             "Please pass a name on the query string or in the request body",
             status_code=400
        )

```

And the corresponding `function.json`:

```json
{
  "scriptFile": "__init__.py",
  "bindings": [
    {
      "authLevel": "function",
      "type": "httpTrigger",
      "direction": "in",
      "name": "req",
      "methods": [
        "get",
        "post"
      ]
    },
    {
      "name": "inputblob",
      "type": "blob",
      "dataType": "string",
      "path": "glofas-zip-file/{name}.csv",
      "connection": "AzureWebJobsStorage",
      "direction": "in"
    },
    {
      "name": "outputblob",
      "type": "blob",
      "dataType": "string",
      "path": "glofas-predictions/glofas_triggers_short.csv",
      "connection": "AzureWebJobsStorage",
      "direction": "out"
    }
  ]
}
```
What we see here is that we have to define both the input file and the output file (which one is input and output can be seen by the `direction` parameter). And we also see in the `__init__.py` that it is nog very evident how to handle these bindings.
So our decision to not use these is twofold: first of all, it is not preferred to define 115 input bindings and 115 output bindings, but the existing code is also easier implemented using the Python-package regarding Azure Blob Storage (azure.storage.blob).

When would defining these bindings then be beneficial? If you only have one input and output file, it is not a real problem to use the official bindings. Also, by using the package, the package must be installed, therefor potentially introducing mismatches between package requirements, and slowing down the code as well. If you have code that takes time to run, the official bindings are preferred over using the package.


### Local vs cloud debugging.
A downside of working with Azure Functions becomes apparent when debugging locally and then pushing to Azure. Sometimes, complications will arise where locally the function runs perfectly, but in Azure some errors do occur. <br>
An example of this is when you are debugging locally, you can write files temporarily to your working directory. In the cloud, you can only write files temporarily in a temp folder. Therefor, it is good to keep these type of differences in mind. <br>
A second example is the use of environment variables. We have already touched on this earlier in this documentation. Azure requires you to set your environment variables one by one in the portal.

### Differences in scheduling time and run time
When scheduling an Azure Function it is likely that there will be some timezone differences between local time and the Azure server. In this project, we have noticed a two hour difference (GMT = CET - 2). This means that we have to account for this time difference when scheduling using a timerTrigger.  

### Database connections in Azure. (IP whitelisting)
In Azure, if you want to make a connection to a database which requires IP whitelisting, you might need to whitelist all the possible IP addresses of your app service. Follow the next steps to find all external IP addresses of your Function app.
```
- Sign in to https://resources.azure.com/
- Select subscriptions > {your subscription} > providers > Microsoft.Web > sites.
- In the JSON panel, find the site with an id property that ends in the name of your function app.
- See outboundIpAddresses and possibleOutboundIpAddresses.
```
It is likely that you do not need to perform the previous steps if you have 'Allow access to Azure services' toggled.

### Extending the pipeline
In order to extend the pipeline to, for example, more countries, the following steps are proposed.
First of all, create a new Function App in the Azure portal as described above. Next, also create a new Function using Visual Studio Code. This can be done using the Azure extension with the button `Create New Project..`. For clarity, it is easier to start this project in a different folder than the existing pipeline.
In essence, from hereon, it is a copy-paste of the previous pipeline.
You can reuse the code from the existing pipeline (copy paste the contents of the folder), and only need to change the parts of the code that are specific for this pipeline. This includes all inputs and outputs (for each Function). For example, when you create a new Function App, it is likely that you also have created a new storage account to write data to. Make sure you refer to this storage account (in the function.json file) in the new pipeline (if that is what you want).
The code itself also refers sometimes to certain locations that need to be updated. An example would be the blob storage location (when we read in the nc-files, e.g.).
After you made sure that all referrals are correct (also note the 'secrets' in the application settings), you can deploy the copied and adapted Functions to the new Function App.

### Useful links
- https://docs.microsoft.com/en-us/azure/azure-functions/
- https://docs.microsoft.com/nl-nl/azure/azure-functions/functions-create-first-function-vs-code?pivots=programming-language-python
- https://docs.microsoft.com/nl-nl/azure/azure-functions/functions-add-output-binding-storage-queue-vs-code?pivots=programming-language-python
- https://docs.microsoft.com/en-us/azure/developer/python/tutorial-vs-code-serverless-python-01
- https://docs.microsoft.com/nl-nl/azure/azure-functions/functions-create-first-azure-function-azure-cli?tabs=bash%2Cbrowser&pivots=programming-language-python
- https://medium.com/@k.giguz/getting-started-with-azure-python-functions-abd4d88706f7
- https://towardsdatascience.com/hosting-your-ml-model-on-azure-functions-ae3ca4ae1232
